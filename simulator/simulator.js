var Pusher = require('pusher');

GLOBAL.pusher = new Pusher({
    appId: '197038',
  	key: 'd94cd30a34fe3e094a04',
  	secret: 'f4c4ab53de91969689be',
  	cluster: 'ap1',
  	encrypted: true
});

boys_array = [];


var start_lat = 12.9248;
var start_long = 77.6274;
var identifier = 0;

piyush_arr = [1,12.9348,77.6274];
boys_array.push(piyush_arr);
dheeraj_arr = [2,12.9138,77.6264];
boys_array.push(dheeraj_arr);
shubham_arr = [3,12.9268,77.6184];
boys_array.push(shubham_arr);
shivam_arr = [4,12.9228,77.6254];
boys_array.push(shivam_arr);
kancha_arr = [5,12.9141,77.6479];
boys_array.push(kancha_arr);
shashank_arr = [6,12.9044,77.6276];
boys_array.push(shashank_arr);
sanchit_arr = [7,12.9248,77.6974];
boys_array.push(sanchit_arr);
debendra_arr = [8,12.8838,77.5914];
boys_array.push(debendra_arr);
abhijeet_arr = [9,12.9249,77.6270];
boys_array.push(abhijeet_arr);
mayank_arr = [10,12.8208,77.8804];
boys_array.push(mayank_arr);

var lat_inc = 0.0001; 
var long_inc = 0.0001; 
var delay = 5000; 

for(var i = 0; i < boys_array.length; i++){
	start(boys_array[i]);	
}

function start(boy_details){
	var coordinates = {
		latitude : boy_details[1],
		longitude : boy_details[2],		
		time : Date.now() + 330*60000,
		identifier : boy_details[0]
	}	
	pusherCall(coordinates);
}

function pusherCall(coordinates){
	pusher.trigger('lastlocation', 'data', {"message": coordinates});	
	setTimeout(function(){ tracker(coordinates) }, delay);
}

function tracker(coords){
	var new_lat = parseFloat(coords['latitude']) + lat_inc;
	var new_long = parseFloat(coords['longitude']) + long_inc;
	var identifier = coords['identifier'];
	var coordinates = {
		latitude : new_lat.toFixed(4),
		longitude : new_long.toFixed(4),		
		time : Date.now() + 330*60000,
		identifier : identifier
	}		
	pusherCall(coordinates);
}